//
//  ViewController.swift
//  User Interface
//
//  Created by cpw on 10/4/17.
//  Copyright © 2017 cpw. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var picker: UIDatePicker!
    
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        pickerDidChange(picker)
    }

    @IBAction func pickerDidChange(_ sender: Any) {
        let date:Date = picker.date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormatter.dateFormat(fromTemplate: "EEE dd-MMM-yyyy", options:
            0, locale: Locale.current)
        let dayOfWeek:String = dateFormatter.string(from: date)
        textField.text = "\(dayOfWeek)"
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

